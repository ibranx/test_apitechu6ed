  const io = require('../io');
  const crypt = require('../crypt');

  const requestJson = require('request-json');
  const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaml6edt/collections/";
  const mLapApiKey = "apiKey=wk7ron7p9hQnmUp1Vq_lmeT6FVAzcetd";

  const fixerURL = "http://data.fixer.io/api/";
  const fixerApiKey = "access_key=101b1bfc0562dcd0ee2672084b81f18a";

  const andorenURL = "https://www.amdoren.com/api/";
  const andorenApiKey = "api_key=zHwYgTt3pywnwDCzCvMFDCKQhYd2fl";

  function getUsersV1(req, res){
    console.log("GET /apitechu/v1/users");

    console.log("Query String");
    console.log("count"+req.query.$count);
    console.log("top"+req.query.$top);

    var users = require('../users_login.json');
    if(req.query.$top != null){
      users = users.slice(0,req.query.$top);
      //users.splice(req.query.$top, users.length - req.query.$top);
    }
    //res.sendFile('usuarios.json', {root: __dirname});
    if(req.query.$count != null && req.query.$count == "true"){
      users.push({
        "count" : users.length
      });
    }
    res.send(users);
  }

  function postUserV1(req, res){
    console.log("POST /apitechu/v1/users");
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
    }
    var users = require('../users_login.json');
    users.push(newUser);
    io.writeUserDataToFile(users);
    res.send({"msg":"Usuario añadido con exito"});

  }

  function deleteUserV1(req, res){
    console.log("DELETE /apitechu/v1/users/"+req.params.id);
    var users = require('../users_login.json');
    //TE QUITA LA POSICION DE UN ELEMENTO INDICADO DENTRO DE UN ARRAY
    var index = -1;
    var found = false;
    for (let user of users) {
      index++;
      console.log("index: "+index+", user : "+user.id);
      if(user.id==req.params.id){
        console.log("found");
        found = true;
        break;
      }
    }
    if(found){
      users.splice(index, 1);
      io.writeUserDataToFile(users);
      res.send({"msg":"Usuario deleteado con exito"});
    } else {
      res.send({"msg":"Usuario no encontrado"});
    }
    //delete users[pos];
    //users.splice(req.params.id - 1, 1);
  }

  function getUsersV2(req, res){
    console.log("GET /apitechu/v2/users");
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.get("user?" + mLapApiKey,
      function(err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios."
        }
        res.send(response);
      }
    );
  }

  function getUsersByIdV2(req, res){
    console.log("GET /apitechu/v2/users/:id");
    var isInteger = false;
    isInteger = Number.isInteger(req.params.id);
    var id = req.params.id;
    var query = '';
    if(isInteger){
      query = 'q={"id" : ' + id + '}';
    } else {
      query = 'q={"id" : "' + id + '"}';
    }
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.get("user?" + query + "&" + mLapApiKey,
      function(err, resMLab, body){
        if(err){
            var response = {
              "msg" : "Error obteniendo usuarios"
            }
            res.status(500);
        } else {
          if(body.length > 0){
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }

  function postUserV2(req, res){
    console.log("POST /apitechu/v2/users/");

    var newUser = {
      "id" : req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    }
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.post("user?" + mLapApiKey, newUser ,
      function(err, resMLab, body){
        console.log("usuario creado correctamente");
        res.send({"msg":"usuario creado con exito"});
      }
    );
  }

  function getAccountsByUserIdV2(req, res){
    console.log("GET /apitechu/v2/users/:id/accounts");
    var isInteger = false;
    isInteger = Number.isInteger(req.params.id);
    var id = req.params.id;
    var query = '';
    if(isInteger){
      query = 'q={"userId" : ' + id + '}';
    } else {
      query = 'q={"userId" : "' + id + '"}';
    }
    console.log(query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.get("accounts?" + query + "&" + mLapApiKey,
      function(err, resMLab, body){
        if(err){
            var response = {
              "msg" : "Error obteniendo usuarios"
            }
            res.status(500);
        } else {
          if(body.length > 0){
            console.log(body);
            var response = body;
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }

  function getGetMovementsByAccountsByUserIdV2(req, res){
    console.log("GET /apitechu/v2/users/:id/accounts/:account/movements");
    var isInteger = false;
    isInteger = Number.isInteger(req.params.id);
    var id = req.params.id;
    var query = '';
    var account = req.params.account;
    if(isInteger){
      query = 'q={"userId" : ' + id + ', "iban" : "'+ account +'"}';
    } else {
      query = 'q={"userId" : "' + id + '", "iban" : "'+ account +'"}';
    }
    console.log(query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.get("accounts?" + query + "&" + mLapApiKey,
      function(err, resMLab, body){
        if(err){
            var response = {
              "msg" : "Error obteniendo usuarios"
            }
            res.status(500);
        } else {
          if(body.length > 0){
            console.log(body);
            var response = body;
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }

  function postMovementV2(req, res){
    console.log("POST /apitechu/v2/movement");
    console.log(req.body);

    var newMovement = {
      "concept" : req.body.concept,
      "movement" : parseFloat(req.body.movement)
    }
    console.log(newMovement);

    var id = req.body.id;
    var account = req.body.account;
    var isInteger = false;
    isInteger = Number.isInteger(req.params.id);
    var query = '';
    if(isInteger){
      query = 'q={"userId" : ' + id + ', "iban" : "'+ account +'"}';
    } else {
      query = 'q={"userId" : "' + id + '", "iban" : "'+ account +'"}';
    }
    console.log(query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.get("accounts?" + query + "&" + mLapApiKey,
      function(err, resMLab, body){
        if(err){
            var response = {
              "msg" : "Error obteniendo usuarios"
            }
            res.status(500);
        } else {
          if(body.length > 0){
            var response = body[0];
            var movs = response.movements;
            var balance = 0;
            movs.push(newMovement);
            for (let mov of movs) {
              console.log(mov.movement);
              balance += mov.movement;
            }
            response.balance = balance;
            console.log(balance);
            response.movements = movs;
            console.log(response);

            //var query = 'q={"userId" : ' + id + ', "iban" : "'+ account +'"}';
            var httpClient = requestJson.createClient(baseMLabURL);
            console.log("client created");
            httpClient.put("accounts?" + query + "&" + mLapApiKey, response ,
              function(err, resMLab, body){
                console.log("cuenta modificada correctamente");
              }
            );

          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }

  function postAccountV2(req, res){
    console.log("POST /apitechu/v2/account");
    console.log(req.body);

    var newAccount = {
      "userId" : req.body.userId,
      "iban" : req.body.iban,
      "balance": 0,
      "badge": "$",
      "movements": []
    }
    console.log(newAccount);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("client created");
    httpClient.post("accounts?" + mLapApiKey, newAccount ,
      function(err, resMLab, body){
        console.log("cuenta creada correctamente");
        res.send({"msg":"cuenta creado con exito", "userId": req.body.userId });
      }
    );
  }

  function getConversionV2(req, res){
    console.log("GET apitechu/v2/conversion/:badge/:amount");
    console.log("badge "+req.params.badge);
    console.log("amount "+req.params.amount);

    var badge = req.params.badge;
    var amount = req.params.amount;
    var fromBadge;
    var toBadge;
    if(badge == "$"){
      fromBadge = "USD";
      toBadge = "EUR";
    } else {
      fromBadge = "EUR";
      toBadge = "USD";
    }
    //&from=EUR&to=USD&amount=50
    //var query = "&from=USD&to=EUR&amount=45";
    var query = "&from="+fromBadge+"&to="+toBadge+"&amount="+amount;
    var httpClient = requestJson.createClient(andorenURL);
    console.log("client created");
    httpClient.get("currency.php?" + andorenApiKey + query,
      function(err, resMLab, body){
        if(err){
            var response = {
              "msg" : "Error obteniendo datos"
            }
            res.status(500);
        } else {
          console.log(body);
          console.log(body.amount);
          if(body.amount > 0){
            var response = body;
          } else {
            var response = {
              "msg" : "Datos no encontrados"
            }
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }

  module.exports.getUsersV1 = getUsersV1;
  module.exports.postUserV1 = postUserV1;
  module.exports.deleteUserV1 = deleteUserV1;
  module.exports.getUsersV2 = getUsersV2;
  module.exports.getUsersByIdV2 = getUsersByIdV2;
  module.exports.postUserV2 = postUserV2;
  module.exports.getAccountsByUserIdV2 = getAccountsByUserIdV2;
  module.exports.getGetMovementsByAccountsByUserIdV2 = getGetMovementsByAccountsByUserIdV2;
  module.exports.postMovementV2 = postMovementV2;
  module.exports.postAccountV2 = postAccountV2;
  module.exports.getConversionV2 = getConversionV2;
