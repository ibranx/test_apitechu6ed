  const io = require('../io');
  const crypt = require('../crypt');

  const requestJson = require('request-json');
  const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaml6edt/collections/";
  const mLapApiKey = "apiKey=wk7ron7p9hQnmUp1Vq_lmeT6FVAzcetd";

  function postLoginV1(req, res){
    var message = {
      "mensaje":"Login incorrecto."
    };
    console.log("Body");
    console.log(req.body.email);
    console.log(req.body.password);
    var users = require('../users_login.json');
    if(req.body.email != null && req.body.password){
      for (user of users) {
        console.log("mail : "+user.email);
        if(user.email==req.body.email){
          console.log("found");
          console.log("password : "+user.password);
          if(user.password == req.body.password){
            message.mensaje = "Login correcto.";
            message.userId = user.id;
            user.logged = true;
          }
        }
      }
    }

    io.writeUserDataToFile(users);
    res.send(message);
  }

  function postLogoutV1(req, res){
    var message = {
      "mensaje":"Logout incorrecto"
    };
    console.log("Body");
    console.log(req.body.id);

    var users = require('../users_login.json');
    if(req.body.id != null){
      for (user of users) {
        console.log("userId : "+user.id);
        if(user.id==req.body.id && user.logged==true){
          console.log("found");
          message.mensaje = "Logout correcto";
          delete user.logged;
        }
      }
    }

    io.writeUserDataToFile(users);
    res.send(message);
  }

  function postLoginV2(req, res){

    console.log("postLoginV2");
    console.log(req.body.email);
    console.log(req.body.password);
    var message = {
      "mensaje":"Login incorrecto"
    };
    var query = 'q={"email" : "' + req.body.email + '"}';
    var httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get("user?" + query + "&" + mLapApiKey,
      function(err, resMLab, body){
        if(err){
            message = {
              "msg" : "Error obteniendo usuarios"
            }
            res.status(500);
        } else {
          if(body.length > 0){
            message = body[0];
            if (crypt.checkPassword(req.body.password, body[0].password)){
              message = {
                "msg" : "login realizado correctamente",
                "userId" : body[0].id,
                "last_name" : body[0].last_name,
                "first_name" : body[0].first_name
              }
              var queryPut = 'q={"id" : ' + body[0].id + '}';
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + queryPut + "&" + mLapApiKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut){
                  console.log(bodyPut);
                }
              );
            } else {
              message = {
                "msg" : "la contraseña no existe"
              }
            }
          } else {
            message = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(message);
      }
    );
  }

  function postLogoutV2(req, res){
    var message = {
      "msg":"Logout incorrecto"
    };
    console.log("Id");
    console.log(req.params.id);
    var id = req.params.id;
    var query = 'q={"id" : ' + id + ', "logged" : true }';
    var httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get("user?" + query + "&" + mLapApiKey,
      function(err, resMLab, body){
        console.log(body[0]);
        if(body[0] == null){
          console.log("Usuario no encontrado");
          message = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        } else {
          console.log(body[0].id);
          console.log(body[0].logged);
          console.log("Logout realizado");
          message = {
            "msg" : "Logout realizado"
          }
          var queryPut = 'q={"id" : ' + body[0].id + '}';
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + queryPut + "&" + mLapApiKey, JSON.parse(putBody),
            function(errPut, resMLabPut, bodyPut){
              console.log(bodyPut);
            }
          );
        }
        res.send(message);
      }
    );
  }

  module.exports.postLoginV1 = postLoginV1;
  module.exports.postLogoutV1 = postLogoutV1;
  module.exports.postLoginV2 = postLoginV2;
  module.exports.postLogoutV2 = postLogoutV2;
