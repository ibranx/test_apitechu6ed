  const express = require('express');
  const app = express();
  const port = process.env.PORT || 3000;
  const bodyParser = require('body-parser');
  const userController = require('./controller/user-controller');
  const authController = require('./controller/auth-controller');

  app.use(bodyParser.json());
  app.listen(port);

  console.log("Estamos escuchando los puertos: "+port);

  //TEST
  app.get('/apitechu/v1/hello',
    function(req, res){
      console.log("GET /apitechu/v1/hello");
      res.send({"msg":"salida GET /apitechu/v1/hello"});
    }
  );

  //POST MONSTRUO
  app.post('/apitechu/v1/monster/:p1/:p2',
    function(req, res){
      console.log("post /apitechu/v1/monster");
      console.log("Parameters");
      console.log(req.params);
      console.log("Query String");
      console.log(req.query);
      console.log("Headers");
      console.log(req.headers);
      console.log("Body");
      console.log(req.body);

      res.send("post /apitechu/v1/monster");
  });

  //POST LOGIN v1
  app.post('/apitechu/v1/login', authController.postLoginV1);

  //POST LOGOUT v1
  app.post('/apitechu/v1/logout/', authController.postLogoutV1);

  //POST LOGIN v2
  app.post('/apitechu/v2/login', authController.postLoginV2);

  //POST LOGIN v2
  app.post('/apitechu/v2/logout/:id', authController.postLogoutV2);

  //GET USERS v1
  app.get('/apitechu/v1/users',   userController.getUsersV1);

  //POST USERS v1
  app.post('/apitechu/v1/users', userController.postUserV1);

  //DELETE USERS v1
  app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

  //GET USERS mlab v2
  app.get('/apitechu/v2/users', userController.getUsersV2);

  //GET USERS mlab v2 id
  app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2);

  //GET ACCOUNTS
  app.get('/apitechu/v2/users/:id/accounts', userController.getAccountsByUserIdV2);

  //GET CONVERSION
  app.get('/apitechu/v2/conversion/:badge/:amount', userController.getConversionV2);

  //GET MOVEMENTS
  app.get('/apitechu/v2/users/:id/accounts/:account/movements', userController.getGetMovementsByAccountsByUserIdV2);

  //POST MOVEMENT
  app.post('/apitechu/v2/movement', userController.postMovementV2);

  //POST ACCOUNT
  app.post('/apitechu/v2/account', userController.postAccountV2);

  //POST USERS v2
  app.post('/apitechu/v2/users', userController.postUserV2);
