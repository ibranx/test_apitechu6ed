  const bcrypt = require('bcrypt');

  function hash(data){
    console.log("hash");
    return bcrypt.hashSync(data, 10);
  }

  function checkPassword(sentPass, userHashedPass){
    console.log("checking pass");
    return bcrypt.compareSync(sentPass, userHashedPass);
  }

  module.exports.hash = hash;
  module.exports.checkPassword = checkPassword;
