  const mocha = require('mocha');
  const chai = require('chai');
  const chaihttp = require('chai-http');

  chai.use(chaihttp);

  var should = chai.should();
  //lanza la api, sin necesidad de que este corriendo
  var server = require('../server');

  //permite aserciones
  var should = chai.should();

  describe("Firts unit test",
    function(){
      it("Test browser",
        function(done){
          //chai.request('https://developer.mozilla.org/en-US/astastasd')
          chai.request('https://www.google.es')
          .get('/')
          .end(
            function(err, res){
              console.log("Resquest finisher");
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);
              done();
            }
          )
        }
      );
    }
  );

  describe("Test api usuarios",
    function(){
      it("Test api hello",
        function(done){
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function(err, res){
              console.log("Resquest has finished");
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);
              done();
            }
          )
        }
      ),
      it("Test api usuarios",
        function(done){
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err, res){
              console.log("Resquest has finished");
              res.should.have.status(200);
              //console.log(res.body);
              res.body.should.be.a("array");
              for(user of res.body){
                user.should.have.property("email");
                user.should.have.property("password");
              }
              done();
            }
          )
        }
      )
    }
  );
